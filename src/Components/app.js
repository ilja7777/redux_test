import React from 'react';
import Counter from "./loading";
import ItemList from "./ItemList";

const App = () => {
    return (<div>
        <Counter/>
        <ItemList/>
    </div>)
};
export default App;