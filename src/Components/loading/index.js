import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import {LoadFetchData} from '../../actions/loading';

const Loader = ({result, FetchData}) => {
    console.log(result)
  return (
      <div>
          <h2>{result}</h2>
          <button
              onClick={()=>{
                 FetchData('http://localhost:3000/a.json');
              }}  className="btn btn-primary btn-lg">Загрузить</button>
      </div>
  )
};

const mapStateToProps = (state) => {
    return {
        result: state.loaded
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        FetchData: (url)=>dispatch(LoadFetchData(url))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Loader);
