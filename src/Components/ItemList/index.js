import React, {Component} from 'react';
import { connect } from 'react-redux';
import { itemsFetchData } from '../../actions/items';

class ItemList extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            hasErrored: false,
            isLoading: false,
        };
    }

    componentDidMount() {
        this.props.fetchData('http://5826ed963900d612000138bd.mockapi.io/items')
    }

    render () {
        const {hasErrored, isLoading, items} = this.props;
        if (hasErrored) {
            return (<p>Ошибка загрузки</p>);
        }
        if (isLoading){
            return (<p>Загрузка...</p>);
        }
        return (
            <ul>
                {items.map((item) => (
                    <li key={item.id}>
                        {item.label}
                    </li>
                ))}
            </ul>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        items: state.items,
        hasErrored: state.itemsHasErrored,
        isLoading: state.itemsIsLoading
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(itemsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);

//export default ItemList;
