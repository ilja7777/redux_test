import {loaded} from "./loading";

export function itemsHasErrored(bool) {
    return {
        type: 'ITEMS_HAS_ERRORED',
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type: 'ITEMS_IS_LOADING',
        isLoading: bool
    };
}

export function itemsFetchDataSuccess(items) {
    return {
        type: 'ITEMS_FETCH_DATA_SUCCESS',
        items
    };
}

export function errorAfterFiveSeconds() {
    // We return a function instead of an action object
    return (dispatch) => {
        setTimeout(() => {
            // This function is able to dispatch other action creators
            dispatch(itemsHasErrored(true));
        }, 5000);
    };
}

export function itemsFetchData(url) {
    return async (dispatch) => {
        try {
            dispatch(itemsIsLoading(true));
            let response = await fetch(url);
            dispatch(itemsIsLoading(false));
            let items = await  response.json();
            dispatch(itemsFetchDataSuccess(items));
        }
        catch (e) {
            dispatch(itemsHasErrored(true))
        }
    };
}

