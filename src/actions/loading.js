import {itemsFetchDataSuccess, itemsHasErrored, itemsIsLoading} from "./items";

export const loading = () => ({type: 'LOAD_LOADING'});
export const loaded = (items) => {
    return ({type: 'LOAD_LOADED', result: items});
}
export const error = () => ({type: 'LOAD_ERROR'});


class myFetch {
    constructor(url){
        this.url = url;
    }
    then(resolve, reject) {
        let response = fetch(this.url);
        setTimeout(()=>resolve(response),3000);
    }
}


export function LoadFetchData(url) {
    return async (dispatch) => {
        try {
            dispatch(loading());
            let response = await (new myFetch(url));
             let res = await  response.json();
             dispatch(loaded(res));
             return res;
        } catch (e) {
            dispatch(error());
            throw new Error('Error');
        }
    };
}
