export const reducer = (state = 'Загрузите объект', action) => {
    switch (action.type) {
        case 'loading':
            return 'Загружается'
        case 'loaded':
            return 'Успешно завершенно'
        case 'error':
            return 'Ошибка'

        default:
            return state;
    }
}