import { combineReducers } from 'redux';
import { items, itemsHasErrored, itemsIsLoading } from './items';
import {loading, loaded, error} from "./loading";

export default combineReducers({
    items,
    itemsHasErrored,
    itemsIsLoading,
    loaded
});