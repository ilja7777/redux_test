export function loaded(state = '', action) {
    switch (action.type) {
        case 'LOAD_LOADED':
            return 'Успешно завершенно';
        case 'LOAD_LOADING':
            return 'Загружается';
        case 'LOAD_ERROR':
            return 'Ошибка';

        default:
            return state;
    }
}

